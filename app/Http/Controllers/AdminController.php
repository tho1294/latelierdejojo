<?php

namespace App\Http\Controllers;

use App\Information;
use App\Product;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.home');
    }

    public function information()
    {
        $products = Product::All();
        $descriptionperso = Information::find("descriptionperso");
        $service1 = Information::find("service1");
        $service2 = Information::find("service2");
        $service3 = Information::find("service3");
        $service4 = Information::find("service4");
        $service5 = Information::find("service5");
        $contact = Information::find("contact");
        $adresse = Information::find("adresse");
        $telephone = Information::find("telephone");
        $mail = Information::find("mail");

        return view('admin.information', compact('products',
            'descriptionperso',
            'service1',
            'service2',
            'service3',
            'service4',
            'service5',
            'contact',
            'adresse',
            'telephone',
            'mail'));
    }

    public function image()
    {
        $images = Product::orderBy('position', 'asc')->get();
        return view('admin.image', compact('images'));
    }

    public function mailbox()
    {
        return view('admin.mailbox');
    }


    public function saveImage(Request $request)
    {
        if ($request->hasFile('image')) {
            $maxPos = Product::max('position') + 1;
            $product = new Product();
            $product->name = $request->input('name');
            $name = $request->file('image')->store('public/uploads/image/produit');
            $path = str_replace("public/uploads/image/produit/", "", $name);
            $product->path = $path;
            $product->position = $maxPos;
            $product->save();
        }

        return redirect('/admin/image');
    }

    public function deleteImage(Request $request, $id)
    {
        $p = Product::find($id);
        $pos = $p->position;
        $p->delete();

        $products = Product::where('position', '>=', $pos)->get();
        foreach ($products as $p) {
            $p->position--;
            $p->save();
        }

        return redirect('/admin/image');
    }

    public function updateInformation(Request $request)
    {
        $descriptionperso = Information::find("descriptionperso");
        $descriptionperso->nom = $request->input('descriptionpersonom');
        $descriptionperso->information = $request->input('descriptionpersoinfo');
        $descriptionperso->save();

        $service1 = Information::find("service1");
        $service1->nom = $request->input('service1nom');
        $service1->information = $request->input('service1info');
        $service1->save();

        $service2 = Information::find("service2");
        $service2->nom = $request->input('service2nom');
        $service2->information = $request->input('service2info');
        $service2->save();

        $service3 = Information::find("service3");
        $service3->nom = $request->input('service3nom');
        $service3->information = $request->input('service3info');
        $service3->save();

        $service4 = Information::find("service4");
        $service4->nom = $request->input('service4nom');
        $service4->information = $request->input('service4info');
        $service4->save();

        $service5 = Information::find("service5");
        $service5->nom = $request->input('service5nom');
        $service5->information = $request->input('service5info');
        $service5->save();

        $contact = Information::find("contact");
        $contact->nom = $request->input('contactnom');
        $contact->information = $request->input('contactinfo');
        $contact->save();

        $adresse = Information::find("adresse");
        $adresse->nom = $request->input('adressenom');
        $adresse->information = $request->input('adresseinfo');
        $adresse->save();

        $telephone = Information::find("telephone");
        $telephone->nom = $request->input('telephonenom');
        $telephone->information = $request->input('telephoneinfo');
        $telephone->save();

        $mail = Information::find("mail");
        $mail->nom = $request->input('mailnom');
        $mail->information = $request->input('mailinfo');
        $mail->save();

        return redirect('/admin/information');
    }

    public function updateImage(Request $request, $id)
    {
        $name = $request->input('name');
        $position = $request->input('position');
        $product = Product::find($id);
        $product->name = $name;
        $pAtPos = Product::where('position', '=', $position)->count();
        if ($pAtPos == 0) {
            $product->position = $position;
        } else {
            if ($position != $product->position) {
                $products = Product::where('position', '>=', $position)->get();
                foreach ($products as $p) {
                    $p->position++;
                    $p->save();
                }
                $product->position = $position;
            }
        }
        $product->save();

        $products = Product::orderBy('position', 'asc')->get();
        $i = 1;
        foreach ($products as $p){
            $p->position = $i;
            $p->save();
            $i++;
        }

        return redirect('/admin/image');
    }
}
