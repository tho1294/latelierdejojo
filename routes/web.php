<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Information;
use App\Product;
use Faker\Provider\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

Route::get('/', function () {
    /*$products = array();;
    $fileList = glob(public_path() . '/uploads/image/produit/*');
    foreach ($fileList as $file) {
        $product = new Product();
        $product->path = str_replace(public_path().'/uploads/image/produit/','' , $file);
        array_push($products,$product);
    }*/

    $products = Product::orderBy('name', 'asc')->get();
    $descriptionperso = Information::find("descriptionperso");
    $service1 = Information::find("service1");
    $service2 = Information::find("service2");
    $service3 = Information::find("service3");
    $service4 = Information::find("service4");
    $service5 = Information::find("service5");
    $contact = Information::find("contact");
    $adresse = Information::find("adresse");
    $telephone = Information::find("telephone");
    $mail = Information::find("mail");
    return view('welcome', compact('products',
        'descriptionperso',
        'service1',
        'service2',
        'service3',
        'service4',
        'service5',
        'contact',
        'adresse',
        'telephone',
        'mail'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->middleware('auth');
Route::get('/admin/mailbox', 'AdminController@mailbox')->middleware('auth');
Route::get('/admin/image', 'AdminController@image')->middleware('auth');

Route::get('/admin/information', 'AdminController@information')->middleware('auth');
Route::post('/admin/updateInformation', 'AdminController@updateInformation')->middleware('auth')->name('updateInfo');
Route::post('/admin/updateImage/{id}', 'AdminController@updateImage')->middleware('auth')->name('updateImage');
Route::post('/admin/saveImage', 'AdminController@saveImage')->middleware('auth')->name('saveImage');
Route::post('/admin/deleteImage/{id}', 'AdminController@deleteImage')->middleware('auth')->name('deleteImage');

