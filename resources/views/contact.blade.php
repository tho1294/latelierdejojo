<!-- contact start -->
<div class="content contact" id="menu-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="templatemo_contactmap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d45277.64144146891!2d6.495469082143168!3d44.82456764909668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4789fd78093abc6f%3A0x249f8cbbd7eb5236!2s05120+Les+Vigneaux!5e0!3m2!1sfr!2sfr!4v1542025562947"
                            width="330" height="385" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <img src="{{ URL::to('/') }}/public/images/templatemo_contactiframe.png" alt="contact map">
                </div>
            </div>
            <div class="col-md-3 col-sm-12 leftalign">
                <div class="templatemo_contacttitle">{{$contact->nom}}</div>
                <div class="clear"></div>
                <p>{{$contact->information}}</p>
                <div class="templatemo_address">
                    <ul>
                        <li class="left fa fa-map-marker"></li>
                        <li class="right">{{$adresse->information}}</li>
                        <li class="clear"></li>
                        <li class="left fa fa-phone"></li>
                        <li class="right">{{$telephone->information}}</li>
                        <li class="clear"></li>
                        <li class="left fa fa-envelope"></li>
                        <li class="right">{{$mail->information}}</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <form role="form">
                    <div class="templatemo_form">
                        <input name="fullname" type="text" class="form-control" id="fullname"
                               placeholder="Votre Nom" maxlength="40">
                    </div>
                    <div class="templatemo_form">
                        <input name="email" type="text" class="form-control" id="email" placeholder="Votre Email"
                               maxlength="40">
                    </div>
                    <div class="templatemo_form">
                        <input name="subject" type="text" class="form-control" id="subject" placeholder="Sujet"
                               maxlength="40">
                    </div>
                    <div class="templatemo_form">
                            <textarea name="message" rows="10" class="form-control" id="message"
                                      placeholder="Message"></textarea>
                    </div>
                    <div class="templatemo_form">
                        <button type="button" onclick="myFunction()" class="btn btn-primary">Evoyer</button>
                    </div>

                    <script>
                        function myFunction() {
                            var input = document.getElementById('email');
                            email = input.value;
                            var input = document.getElementById('subject');
                            subject = input.value;
                            var input = document.getElementById('message');
                            message = input.value;
                            window.location.href = "mailto:" + "contact@latelierdejojo.com" + "?subject=" + subject+"&body="+message;
                        }
                    </script>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- contact end -->