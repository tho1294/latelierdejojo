<!-- gallery start -->
<div class="content homepage" id="menu-1">
    <div class="container">
        <div class="row templatemorow">
            <div class="hex col-sm-6">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2"
                                 style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[0]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[0]->path}}"
                                       data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[1]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[1]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6  templatemo-hex-top2">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[2]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[2]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6  templatemo-hex-top3">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[3]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[3]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6  templatemo-hex-top3">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[4]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[4]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6 hex-offset templatemo-hex-top1 templatemo-hex-top2">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[5]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[5]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6 templatemo-hex-top1 templatemo-hex-top3">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[6]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[6]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top3">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[7]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[7]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top2">
                <div>
                    <div class="hexagon hexagon2 gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2" style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[8]->path}});">
                                <div class="overlay">
                                    <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[8]->path}}" data-rel="lightbox" class="fa fa-expand"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container">
        <div class="row templatemorow">
            @for ($i = 0; $i < 9; $i++)
                @if ($i < 3)
                    <div class="hex col-sm-6">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif($i < 4 )
                    <div class="hex col-sm-6  templatemo-hex-top3">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif($i < 5 )
                    <div class="hex col-sm-6  templatemo-hex-top3">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif($i == 5 )
                    <div class="hex col-sm-6 hex-offset templatemo-hex-top1 templatemo-hex-top2">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif( $i == 8 )
                    <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top2">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif($i == 6 || $i == 7 )
                    <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top3">
                        <div>
                            <div class="hexagon hexagon2 gallery-item">
                                <div class="hexagon-in1">
                                    <div class="hexagon-in2"
                                         style="background-image:  url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                        <div class="overlay">
                                            <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                               data-rel="lightbox"
                                               class="fa fa-expand"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endfor
        </div>
    </div>--}}

    <div id="newpost" style="display:none;">
        <div class="container answer_list templatemo_gallerytop">
            <div class="row templatemorow">
                @for ($i = 10; $i < count($products); $i++)
                    @switch(($i-1)%9)
                        @case(0)
                        <div class="hex col-sm-6">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(1)
                        <div class="hex col-sm-6">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(2)
                        <div class="hex col-sm-6  templatemo-hex-top2">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(3)
                        <div class="hex col-sm-6  templatemo-hex-top3">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(4)
                        <div class="hex col-sm-6  templatemo-hex-top3">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(5)
                        <div class="hex col-sm-6 hex-offset templatemo-hex-top1 templatemo-hex-top2">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(6)
                        <div class="hex col-sm-6 templatemo-hex-top1 templatemo-hex-top3">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(7)
                        <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top3">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(8)
                        <div class="hex col-sm-6 templatemo-hex-top1  templatemo-hex-top2">
                            <div>
                                <div class="hexagon hexagon2 gallery-item">
                                    <div class="hexagon-in1">
                                        <div class="hexagon-in2"
                                             style="background-image: url({{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}});">
                                            <div class="overlay">
                                                <a href="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$products[$i]->path}}"
                                                   data-rel="lightbox"
                                                   class="fa fa-expand"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                        @default
                    @endswitch
                    @if(($i-1)%9 == 8)
            </div>
        </div>

        <div class="container answer_list templatemo_gallerytop">
            <div class="row templatemorow">
                @endif
                @endFor
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="templatemo_loadmore">
                <button class="gallery_more" id="button" onClick="showhide()">Voir plus</button>
            </div>
        </div>
    </div>
</div>
<!-- gallery end -->