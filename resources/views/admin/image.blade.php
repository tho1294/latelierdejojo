@extends('admin.layout')

@section('content')
    <div class="box box-primary">

        <!-- form start -->
        <form action="{{ route('saveImage') }}" method="post" enctype='multipart/form-data'>
            {{ csrf_field() }}

            <div class="box-body">
                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="nom">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input name="image" type="file" id="exampleInputFile">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </form>


    </div>
    <div class="box box-primary">
        <div class="container">
            <div class="panel-group">
                @foreach($images as $image)
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-1">
                                    <p>{{$image->position}}</p>
                                </div>
                                <div class="col-sm-1">
                                    <img style="object-fit: cover; width:100%; height: 115px; margin:20px;"
                                         src="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$image->path}}">
                                </div>
                                <div class="col-sm-4">
                                    <p>{{$image->name}}</p>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#{{$image->id}}">Editer
                                    </button>

                                    <form action="{{ route('deleteImage',$image->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="{{$image->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{$image->name}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('updateImage',$image->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img style="object-fit: cover; width:100%; margin:20px;"
                                                     src="{{ URL::to('/') }}/public/storage/uploads/image/produit/{{$image->path}}">
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for="name{{$image->id}}">nom</label>
                                                    <input type="text" id="name{{$image->id}}" class="form-control"
                                                           name="name" placeholder="nom" value="{{$image->name}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="position{{$image->id}}">position</label>
                                                    <input type="number" id="position{{$image->id}}"
                                                           class="form-control" name="position" placeholder="positio,"
                                                           value="{{$image->position}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer
                                        </button>
                                        <input type="submit" class="btn btn-primary" value="Enregistrer"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection