@extends('admin.layout')

@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <form action="{{ route('updateInfo') }}" method="post">
                {{ csrf_field() }}

                <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">

                    <!-- form start -->

                    <div class="box-body">
                        <h3>Qui suis-je?</h3>
                        <div class="form-group">
                            <label for="descriptionpersonom">Description</label>
                            <input type="text" class="form-control" name="descriptionpersonom" id="descriptionpersonom"
                                   value="{{$descriptionperso->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="descriptionpersoinfo">Description</label>
                            <input type="text" class="form-control" name="descriptionpersoinfo" id="descriptionpersoinfo"
                                   value="{{$descriptionperso->information}}">
                        </div>


                        <h3>Contact</h3>
                        <div class="form-group">
                            <label for="contactnom">Description</label>
                            <input type="text" class="form-control" name="contactnom" id="contactnom" value="{{$contact->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="contactinfo">Description</label>
                            <input type="text" class="form-control" name="contactinfo" id="contactinfo"
                                   value="{{$contact->information}}">
                        </div>


                        <h3>Adresse</h3>
                        <div class="form-group">
                            <label for="adressenom">Description</label>
                            <input type="text" class="form-control" name="adressenom" id="adressenom" value="{{$adresse->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="adresseinfo">Description</label>
                            <input type="text" class="form-control" name="adresseinfo" id="adresseinfo"
                                   value="{{$adresse->information}}">
                        </div>


                        <h3>Telephone</h3>
                        <div class="form-group">
                            <label for="telephonenom">Description</label>
                            <input type="text" class="form-control" name="telephonenom" id="telephonenom" value="{{$telephone->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="telephoneinfo">Description</label>
                            <input type="text" class="form-control" name="telephoneinfo" id="telephoneinfo"
                                   value="{{$telephone->information}}">
                        </div>


                        <h3>Mail</h3>
                        <div class="form-group">
                            <label for="mailnom">Description</label>
                            <input type="text" class="form-control" name="mailnom" id="mailnom" value="{{$mail->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="mailinfo">Description</label>
                            <input type="text" class="form-control" name="mailinfo" id="mailinfo" value="{{$mail->information}}">
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">modifier</button>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <div class="box-body">

                        <h3>Service 1</h3>
                        <div class="form-group">
                            <label for="service1nom">Description</label>
                            <input type="text" class="form-control" name="service1nom" id="service1nom" value="{{$service1->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="service1info">Description</label>
                            <input type="text" class="form-control" name="service1info" id="service1info"
                                   value="{{$service1->information}}">
                        </div>


                        <h3>Service 2</h3>
                        <div class="form-group">
                            <label for="service2nom">Description</label>
                            <input type="text" class="form-control" name="service2nom" id="service2nom" value="{{$service2->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="service2info">Description</label>
                            <input type="text" class="form-control" name="service2info" id="service2info"
                                   value="{{$service2->information}}">
                        </div>


                        <h3>Service 3</h3>
                        <div class="form-group">
                            <label for="service3nom">Description</label>
                            <input type="text" class="form-control" name="service3nom" id="service3nom" value="{{$service3->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="service1info">Description</label>
                            <input type="text" class="form-control" name="service3info" id="service3info"
                                   value="{{$service3->information}}">
                        </div>


                        <h3>Service 4</h3>
                        <div class="form-group">
                            <label for="service4nom">Description</label>
                            <input type="text" class="form-control" name="service4nom" id="service4nom" value="{{$service4->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="service4info">Description</label>
                            <input type="text" class="form-control" name="service4info" id="service4info"
                                   value="{{$service4->information}}">
                        </div>


                        <h3>Service 5</h3>
                        <div class="form-group">
                            <label for="service5nom">Description</label>
                            <input type="text" class="form-control" name="service5nom" id="service5nom" value="{{$service5->nom}}">
                        </div>
                        <div class="form-group">
                            <label for="service5info">Description</label>
                            <input type="text" class="form-control" name="service5info" id="service5info"
                                   value="{{$service5->information}}">
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (left) -->
            </form>
        <!-- /.row -->
        </div>
    </section>
@endsection