<!-- service start -->
<div class="content services" id="menu-3">
    <div class="container">
        <div class="row templatemo_servicerow">
            <div class="templatemo_hexservices col-sm-6">
                <div class="blok text-center">
                    <div class="hexagon-a">
                        <a class="hlinktop" href="#">
                            <div class="hexa-a">
                                <div class="hcontainer-a">
                                    <div class="vertical-align-a">
                                        <span class="texts-a"><i class="fa fa-bell"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hexagonservices">
                        <a class="hlinkbott" href="#">
                            <div class="hexa">
                                <div class="hcontainer">
                                    <div class="vertical-align">
                                        <span class="texts"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="templatemo_servicetext" style="float:none; margin-top:250px;">
                    <h3>{{$service1->nom}}</h3>
                    <p>{{$service1->information}}</p>
                </div>
            </div>
            <div class="templatemo_hexservices col-sm-6">
                <div class="blok text-center">
                    <div class="hexagon-a">
                        <a class="hlinktop" href="#">
                            <div class="hexa-a">
                                <div class="hcontainer-a">
                                    <div class="vertical-align-a">
                                        <span class="texts-a"><i class="fa fa-briefcase"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hexagonservices">
                        <a class="hlinkbott" href="#">
                            <div class="hexa">
                                <div class="hcontainer">
                                    <div class="vertical-align">
                                        <span class="texts"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="templatemo_servicetext" style="float:none; margin-top:250px;">
                    <h3>{{$service2->nom}}</h3>
                    <p>{{$service2->information}}</p>
                </div>
            </div>
            <div class="templatemo_hexservices col-sm-6">
                <div class="blok text-center">
                    <div class="hexagon-a">
                        <a class="hlinktop" href="#">
                            <div class="hexa-a">
                                <div class="hcontainer-a">
                                    <div class="vertical-align-a">
                                        <span class="texts-a"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hexagonservices">
                        <a class="hlinkbott" href="#">
                            <div class="hexa">
                                <div class="hcontainer">
                                    <div class="vertical-align">
                                        <span class="texts"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="templatemo_servicetext" style="float:none; margin-top:250px;">
                    <h3>{{$service3->nom}}</h3>
                    <p>{{$service3->information}}</p>
                </div>
            </div>
            <div class="templatemo_hexservices col-sm-6">
                <div class="blok text-center">
                    <div class="hexagon-a">
                        <a class="hlinktop" href="#">
                            <div class="hexa-a">
                                <div class="hcontainer-a">
                                    <div class="vertical-align-a">
                                        <span class="texts-a"><i class="fa fa-trophy"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hexagonservices">
                        <a class="hlinkbott" href="#">
                            <div class="hexa">
                                <div class="hcontainer">
                                    <div class="vertical-align">
                                        <span class="texts"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="templatemo_servicetext" style="float:none; margin-top:250px;">
                    <h3>{{$service4->nom}}</h3>
                    <p>{{$service4->information}}</p>
                </div>
            </div>
            <div class="templatemo_hexservices col-sm-6">
                <div class="blok text-center">
                    <div class="hexagon-a">
                        <a class="hlinktop" href="#">
                            <div class="hexa-a">
                                <div class="hcontainer-a">
                                    <div class="vertical-align-a">
                                        <span class="texts-a"><i class="fa fa-thumb-tack"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hexagonservices">
                        <a class="hlinkbott" href="#">
                            <div class="hexa">
                                <div class="hcontainer">
                                    <div class="vertical-align">
                                        <span class="texts"></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="templatemo_servicetext" style="float:none; margin-top:250px;">
                    <h3>{{$service5->nom}}</h3>
                    <p>{{$service5->information}}</p>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- service end -->