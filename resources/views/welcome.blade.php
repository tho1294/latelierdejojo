<!DOCTYPE html>
<head>
    <title>L'atelier de Joelle</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <!--
    Polygon Template
    http://www.templatemo.com/tm-400-polygon
    -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset("/public/favicon.ico") }}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset("/public/css/bootstrap.min.css")}}">

    <link rel="stylesheet" href="{{asset("/public/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("/public/css/templatemo_misc.css")}}">
    <link rel="stylesheet" href="{{asset("/public/css/templatemo_style.css")}}">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,600' rel='stylesheet' type='text/css'>

    <script
            src="https://code.jquery.com/jquery-1.10.2.js"
            integrity="sha256-it5nQKHTz+34HijZJQkpNBIHsjpV8b6QzMJs9tmOBSo="
            crossorigin="anonymous"></script>
    <script src="{{asset("/public/js/jquery.lightbox.js")}}"></script>
    <script src="{{asset("/public/js/templatemo_custom.js")}}"></script>
    <script>
        function showhide() {
            var div = document.getElementById("newpost");
            if (div.style.display !== "none") {
                div.style.display = "none";
            }
            else {
                div.style.display = "block";
            }
        }
    </script>

</head>
<body>
<div class="site-header">
    <div class="main-navigation">
        <div class="responsive_menu">
            <ul>
                <li><a class="show-1 templatemo_home" href="#">Galerie</a></li>
                <li><a class="show-2 templatemo_page2" href="#">Qui suis-je ?</a></li>
                <li><a class="show-3 templatemo_page3" href="#">Services</a></li>
                <li><a class="show-5 templatemo_page5" href="#">Contact</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="row templatemo_gallerygap">
                <div class="col-md-12 responsive-menu">
                    <a href="#" class="menu-toggle-btn">
                        <i class="fa fa-bars"></i>
                    </a>
                </div> <!-- /.col-md-12 -->
                <div class="col-md-5 col-sm-12">
                    <!--<a href="#"><img src="{{ URL::to('/') }}/public/images/templatemo_logo.jpg"
                                     alt="Polygon HTML5 Template"></a>-->
                    <p style="font-family: 'Raleway ExtraBold';font-size: 50px; margin-top: 20px; color: #FFFFFF;">
                        L'<span style="color: #B69E40">A</span>t<span style="color: #B69E40">e</span>li<span style="color: #B69E40">e</span>r
                        d<span style="color: #B69E40">e</span>
                        J<span style="color: #B69E40">o</span>J<span style="color: #B69E40">o</span></p>
                </div>
                <div class="col-md-7 main_menu">
                    <ul>
                        <li><a class="show-1 templatemo_home" href="#">
                                <span class="fa fa-camera"></span>
                                Galerie</a></li>
                        <li><a class="show-2 templatemo_page2" href="#">
                                <span class="fa fa-users"></span>
                                Qui suis-je ?</a></li>
                        <li><a class="show-3 templatemo_page3" href="#">
                                <span class="fa fa-cogs"></span>
                                Services</a></li>
                        <li><a class="show-5 templatemo_page5" href="#">
                                <span class="fa fa-envelope"></span>
                                Contact</a></li>
                    </ul>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-navigation -->
</div> <!-- /.site-header -->
<div id="menu-container">
    @include('image')
    @include('team')
    @include('service')
    @include('contact')
    @include('footer')

    <script>
        $(document).ready(function () {
            $('.gallery_more').click(function () {
                var $this = $(this);
                $this.toggleClass('gallery_more');
                if ($this.hasClass('gallery_more')) {
                    $this.text('Voir plus');
                } else {
                    $this.text('Voir moins');
                }
            });
        });
    </script>
    <!-- templatemo 400 polygon -->
</body>
</html>