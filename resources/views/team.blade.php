<!-- team start -->
<div class="content team" id="menu-2">
    <div class="templatemo_ourteam">
        <div class="container templatemo_hexteam">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="hexagon hexagonteam gallery-item">
                        <div class="hexagon-in1">
                            <div class="hexagon-in2"
                                 style="background-image: url({{ URL::to('/') }}/public/images/team/1.jpg);">
                                <div class="overlay templatemo_overlay1">
                                    <a href="#fb">
                                        <div class="smallhexagon">
                                            <span class="fa fa-facebook"></span>
                                        </div>
                                    </a>

                                    <a href="#tw">
                                        <div class="smallhexagon">
                                            <span class="fa fa-twitter"></span>
                                        </div>
                                    </a>

                                    <a href="#ln">
                                        <div class="smallhexagon">
                                            <span class="fa fa-linkedin"></span>
                                        </div>
                                    </a>

                                    <a href="#rs">
                                        <div class="smallhexagon">
                                            <span class="fa fa-rss"></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="clear"></div>
                                <div class="overlay templatemo_overlaytxt">Phasellus interdum</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 templatemo_servicetxt">
                    <h2>{{$descriptionperso->nom}}</h2>
                    <p>{{$descriptionperso->information}}</p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        </br>
        </br>

    </div>
</div>

<!--team end-->